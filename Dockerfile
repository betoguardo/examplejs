FROM node:8.9

RUN apt-get update

RUN apt-get upgrade -y

RUN apt-get install unzip

RUN mkdir -p /app

WORKDIR /app

COPY * /app/

RUN wget https://sonarsource.bintray.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-3.2.0.1227-linux.zip

RUN unzip sonar-scanner-cli-3.2.0.1227-linux.zip
